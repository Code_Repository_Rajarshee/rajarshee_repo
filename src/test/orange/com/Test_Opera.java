package test.orange.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.opera.OperaDriver;

public class Test_Opera {

	public static void main(String[] args) {
		System.setProperty("webdriver.opera.driver",".\\operadriver\\operadriver.exe");
		WebDriver driver = new OperaDriver();
		driver.get("http://newtours.demoaut.com/");
		driver.close();
	}

}
