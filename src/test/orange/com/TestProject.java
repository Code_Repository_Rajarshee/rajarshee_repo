package test.orange.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class TestProject {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.spicejet.com/");
		//driver.manage().window().maximize();
		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_rbtnl_Trip_1']")).isSelected();
		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_rbtnl_Trip_1']")).click();
		WebElement crcy = driver.findElement(By.xpath("//select[@id='ctl00_mainContent_DropDownListCurrency']"));
		Select select3 = new Select(crcy);
		select3.selectByVisibleText("USD");
		WebElement frm = driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_originStation1']"));
		Select select = new Select(frm);
		select.selectByIndex(6);
		WebElement to = driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_destinationStation1']"));
		Select select1 = new Select(to);
		select1.selectByVisibleText("Adampur (AIP)");
		Actions act = new Actions(driver);
		WebElement dept_dt = driver.findElement(By.xpath("//input[@name='ctl00$mainContent$view_date1']"));
		act.moveToElement(dept_dt).click(dept_dt).build().perform();
		WebElement cal = driver.findElement(By.xpath("//span[@class='ui-datepicker-year']"));
		act.moveToElement(cal).click(cal).build().perform();
	}

}
