package test.orange.com;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class Testfilereadingexceptusernandpass {

	public static void main(String[] args) throws IOException, FindFailed, InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		Thread.sleep(1000);
		driver.get("http://newtours.demoaut.com/");
		Thread.sleep(1000);
		WebElement registr = driver.findElement(By.xpath("//a[contains(text(),'REGISTER')]"));
		registr.click();
		Thread.sleep(1000);
		File file = new File(".\\testdata\\testdata.properties");
		FileInputStream fis = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fis);
		Thread.sleep(1000);
		WebElement fname = driver.findElement(By.xpath("//input[@name='firstName']"));
		fname.sendKeys(prop.getProperty("FirstName"));
		Thread.sleep(1000);
		WebElement lname = driver.findElement(By.xpath("//input[@name='lastName']"));
		lname.sendKeys(prop.getProperty("LastName"));
		Thread.sleep(1000);
		WebElement phn = driver.findElement(By.xpath("//input[@name='phone']"));
		phn.sendKeys(prop.getProperty("Phone"));
		Thread.sleep(1000);
		WebElement email = driver.findElement(By.xpath("//input[@id='userName']"));
		email.sendKeys(prop.getProperty("Email"));
		Thread.sleep(1000);
		WebElement add = driver.findElement(By.xpath("//input[@name='address1']"));
		add.sendKeys(prop.getProperty("Address"));
		Thread.sleep(1000);
		WebElement cty = driver.findElement(By.xpath("//input[@name='city']"));
		cty.sendKeys(prop.getProperty("City"));
		Thread.sleep(1000);
		WebElement st = driver.findElement(By.xpath("//input[@name='state']"));
		st.sendKeys(prop.getProperty("State"));
		Thread.sleep(1000);
		WebElement pcode = driver.findElement(By.xpath("//input[@name='postalCode']"));
		pcode.sendKeys(prop.getProperty("PostalCode"));
		Thread.sleep(1000);
		WebElement contry = driver.findElement(By.xpath("//select[@name='country']"));
		Select select = new Select(contry);
		select.selectByVisibleText("INDIA");
		Thread.sleep(1000);
		WebElement uname = driver.findElement(By.xpath("//input[@id='email']"));
		uname.sendKeys(prop.getProperty("UserName"));
		Thread.sleep(1000);
		WebElement pass = driver.findElement(By.xpath("//input[@name='password']"));
		pass.sendKeys(prop.getProperty("Password"));
		Thread.sleep(1000);
		WebElement confpass = driver.findElement(By.xpath("//input[@name='confirmPassword']"));
		confpass.sendKeys(prop.getProperty("ConfirmPassword"));
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='register']")).click();
		Thread.sleep(1000);
		String expurl = "http://newtours.demoaut.com/create_account_success.php";
		String acturl = driver.getCurrentUrl();
		if(expurl.equals(acturl)) {
			System.out.println("Registration Succesfull");
		}else {
			System.out.println("Registration Failed");
		}
		String exptle = "Register: Mercury Tours";
		String acttle = driver.getTitle();
		if(exptle.contains(acttle)) {
			System.out.println("Registration Succesfull");
		}else {
			System.out.println("Registration Failed");
		}
		Thread.sleep(1000);
		WebElement home = driver.findElement(By.xpath("//a[contains(text(),'Home')]"));
		home.click();
		Thread.sleep(1000);
		Screen screen = new Screen();
		Pattern pattern1 = new Pattern(".\\sikulifiles\\UserName.PNG");
		screen.click(pattern1);
		screen.type("dasd");
		Thread.sleep(1000);
		Pattern pattern2 = new Pattern(".\\sikulifiles\\Password.PNG");
		screen.click(pattern2);
		screen.type("dasd");
		Thread.sleep(1000);
		Pattern pattern3 = new Pattern(".\\sikulifiles\\Signin.PNG");
		screen.click(pattern3);
		Thread.sleep(1000);
		WebElement sbmt = driver.findElement(By.xpath("//input[@name='login']"));
		if(sbmt.isDisplayed()) {
			System.out.println("Login Passed");
		}else {
			System.out.println("Login failed");
		}
		Thread.sleep(1000);
		WebElement un = driver.findElement(By.xpath("//input[@name='userName']"));
		un.sendKeys("dasd");
		WebElement pa = driver.findElement(By.xpath("//input[@name='password']"));
		pa.sendKeys("dasd");
		WebElement lg = driver.findElement(By.xpath("//input[@name='login']"));
		lg.click();
		//driver.navigate().to("http://newtours.demoaut.com/mercuryreservation.php");
		WebElement rdb2 = driver.findElement(By.xpath("//input[@value='oneway']"));
		rdb2.click();
		Thread.sleep(1000);
		WebElement passgr = driver.findElement(By.xpath("//select[@name='passCount']"));
		Select select1 = new Select(passgr);
		select1.selectByVisibleText("3");
		Thread.sleep(1000);
		WebElement deptfrm = driver.findElement(By.xpath("//select[@name='fromPort']"));
		Select select2 = new Select(deptfrm);
		select2.selectByValue("London");
		Thread.sleep(1000);
		WebElement on1 = driver.findElement(By.xpath("//select[@name='fromMonth']"));
		Select select3 = new Select(on1);
		select3.selectByIndex(7);
		Thread.sleep(1000);
		WebElement on2 = driver.findElement(By.xpath("//select[@name='fromDay']"));
		Select select4 = new Select(on2);
		select4.selectByVisibleText("13");
		Thread.sleep(1000);
		WebElement toprt = driver.findElement(By.xpath("//select[@name='toPort']"));
		Select select5 = new Select(toprt);
		select5.selectByValue("Sydney");
		Thread.sleep(1000);
		WebElement tomnth = driver.findElement(By.xpath("//select[@name='toMonth']"));
		Select select6 = new Select(tomnth);
		select6.selectByIndex(9);
		Thread.sleep(1000);
		WebElement tody = driver.findElement(By.xpath("//select[@name='toDay']"));
		Select select7 = new Select(tody);
		select7.selectByVisibleText("22");
		Thread.sleep(1000);
		WebElement bclass = driver.findElement(By.xpath("//input[@value='First']"));
		bclass.click();
		Thread.sleep(1000);
		WebElement airlne = driver.findElement(By.xpath("//select[@name='airline']"));
		Select select8 = new Select(airlne);
		select8.selectByVisibleText("Pangea Airlines");
		Thread.sleep(1000);
		WebElement conti = driver.findElement(By.xpath("//input[@name='findFlights']"));
		conti.click();
		Thread.sleep(1000);
		String expurl1 = "http://newtours.demoaut.com/mercuryreservation2.php";
		String acturl1 = driver.getCurrentUrl();
		if(expurl1.equals(acturl1)) {
			System.out.println("Booking Succesfull");
		}else {
			System.out.println("Booking Failed");
		}
		String exptle1 = "Select a Flight: Mercury Tours";
		String acttle1 = driver.getTitle();
		if(exptle1.contains(acttle1)) {
			System.out.println("Selection Succesfull");
		}else {
			System.out.println("Selection Failed");
		}
		driver.close();
	}

}
