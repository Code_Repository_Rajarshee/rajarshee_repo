package test.orange.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class TestSikuliunamepass {

	public static void main(String[] args) throws FindFailed, InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");
		Screen screen = new Screen();
		Pattern pattern1 = new Pattern(".\\sikulifiles\\UserName.PNG");
		screen.click(pattern1);
		screen.type("dasd");
		Pattern pattern2 = new Pattern(".\\sikulifiles\\Password.PNG");
		screen.click(pattern2);
		screen.type("dasd");
		Pattern pattern = new Pattern(".\\sikulifiles\\Signin.PNG");
		screen.click(pattern);
		Thread.sleep(2000);
		driver.close();
	}

}
