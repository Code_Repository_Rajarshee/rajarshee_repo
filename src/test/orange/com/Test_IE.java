package test.orange.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Test_IE {

	public static void main(String[] args) {
		System.setProperty("webdriver.ie.driver", ".\\iedriver\\IEDriverServer.exe");
		WebDriver driver = new InternetExplorerDriver();
		driver.get("http://newtours.demoaut.com/");
		driver.close();
	}

}
