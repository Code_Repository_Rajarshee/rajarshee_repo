package test.orange.com;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Test_Scroll {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.javatpoint.com/");
		JavascriptExecutor js = (JavascriptExecutor)driver;
		//js.executeScript("window.scrollBy(0,1500)");
		WebElement element = driver.findElement(By.xpath("//p[contains(text(),'Selenium')]"));
		js.executeScript("arguments[0].scrollIntoView();", element);
		Thread.sleep(5000);
		js.executeScript("window.scrollBy(5000,document.body.scrollHeight)");
		Thread.sleep(3000);
		driver.close();
	}

}
