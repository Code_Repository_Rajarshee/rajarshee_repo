package test.orange.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TestDragandDrop {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.globalsqa.com/demo-site/draganddrop/#Photo%20Manager");
		Actions act = new Actions(driver);
		driver.switchTo().frame(0);
		WebElement source = driver.findElement(By.xpath("//li[1]//img[1]"));
		WebElement target = driver.findElement(By.xpath("//div[@id='trash']"));
		act.clickAndHold(source).moveToElement(target).release().build().perform();
		//WebElement drag = driver.findElement(By.xpath("//*[@id='draggable']"));
		//WebElement drop = driver.findElement(By.xpath("//*[@id='droppable']"));
		//WebElement source = driver.findElement(By.xpath("//img[@id='sourceImage']"));
		//WebElement target = driver.findElement(By.xpath("//div[@id='targetDiv']"));
	}

}
