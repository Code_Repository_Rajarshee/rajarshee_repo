package test.orange.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Testchkbox {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/");
		WebElement uname = driver.findElement(By.xpath("//input[@id='txtUsername']"));
		uname.sendKeys("Admin");
		WebElement password = driver.findElement(By.xpath("//input[@id='txtPassword']"));
		password.sendKeys("admin123");
		WebElement login = driver.findElement(By.xpath("//input[@id='btnLogin']"));
		login.click();
		WebElement pim = driver.findElement(By.xpath("//b[contains(text(),'PIM')]"));
		Actions action = new Actions(driver);
		action.moveToElement(pim).build().perform();
		action.click(pim).build().perform();
		WebElement chk = driver.findElement(By.xpath("//input[@id='ohrmList_chkSelectAll']"));
		chk.click();
		Thread.sleep(3000);
		driver.close();
	}

}
