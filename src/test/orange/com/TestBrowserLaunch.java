package test.orange.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class TestBrowserLaunch {

	public static void main(String[] args) throws FindFailed, InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");
		WebElement uname = driver.findElement(By.xpath("//input[@name='userName']"));
		uname.sendKeys("dasd");
		WebElement pass = driver.findElement(By.xpath("//input[@name='password']"));
		pass.sendKeys("dasd");
		Screen screen = new Screen();
		Pattern pattern = new Pattern(".\\sikulifiles\\Signin.PNG");
		screen.click(pattern);
		Thread.sleep(2000);
		driver.close();
	}

}
