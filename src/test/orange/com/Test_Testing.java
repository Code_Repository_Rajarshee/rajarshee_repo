package test.orange.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Test_Testing {
	WebDriver driver;
	public static void main(String[] args) throws InterruptedException{
		Test_Testing tslb = new Test_Testing();
		tslb.brwLaunch();
		tslb.login();
		//tslb.validateurl();
		//tslb.validateTitle();
		//tslb.validateWebElement();
		tslb.terminate();
	}

	public void brwLaunch() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		Thread.sleep(5000);
		driver.get("http://newtours.demoaut.com/mercurywelcome.php");	
		driver.manage().window().maximize();
	}
	public void login(){
		WebElement uname = driver.findElement(By.xpath("//input[@name='userName']"));
		uname.sendKeys("dasd");
		WebElement pass = driver.findElement(By.xpath("//input[@name='password']"));
		pass.sendKeys("dasd");
		WebElement login = driver.findElement(By.xpath("//input[@name='login']"));
		login.click();
	}

		public void validateurl() {
			String expurl = "http://newtours.demoaut.com/mercuryreservation.php?osCsid=bdc07ae7620b7ed51831f724b19cd0fe";
			String acturl = driver.getCurrentUrl();
			if(expurl.equals(acturl)) {
				System.out.println("Test Passed for Url");
			}else {
				System.out.println("Test Failed for Url");
			}
		}
		public void validateTitle() {
			String expTil = "Find a Flight: Mercury Tours: ";
			String actTil = driver.getTitle();
			if(actTil.equals(expTil)) {
				System.out.println("Test Passed for Title");
			}else {
				System.out.println("Test Failed for Title");
			}
		}
		public void validateWebElement() {
			WebElement cont = driver.findElement(By.xpath("//input[@name='findFlights']"));
			if(cont.isDisplayed()) {
				System.out.println("Test Passed for webElement");
			}else {
				System.out.println("Test Failed for webElement");
			}
		}
		public void terminate() {
			driver.close();
		}
}
