package test.orange.com;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Test_ReadData {

	WebDriver driver;
	
	public static void main(String[] args) throws IOException, InterruptedException {
		Test_ReadData trd = new Test_ReadData();
		trd.LaunchBrowser();
		trd.loginwithtestdata();
		trd.close();
	}
	
	public void LaunchBrowser() {
		
		System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com/");
	}
	
	public void loginwithtestdata() throws IOException {
	
		File file = new File(".\\testdata\\testdata.properties");
		FileInputStream fis = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fis);
		WebElement uname = driver.findElement(By.xpath("//input[@name='userName']"));
		uname.sendKeys(prop.getProperty("Username"));
		WebElement pass = driver.findElement(By.xpath("//input[@name='password']"));
		pass.sendKeys(prop.getProperty("Password"));
		WebElement login = driver.findElement(By.xpath("//input[@name='login']"));
		login.click();
	}
	
	public void close() throws InterruptedException {
		Thread.sleep(5000);
		driver.close();
	}
	
}
