package test.orange.com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Test_Mozilla {

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", ".\\firefoxdriver\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get("http://newtours.demoaut.com/");
		driver.close();
	}

}
